//programa para agregar un mapa

class Mapa {
    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores = [];
    circulos = [];
    poligonos = [];

    constructor() {
        this.posicionInicial = [4.698256, -74.125811];
        this.escalaInicial = 15;
        this.proveedorURL = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor = {
            maxZoom: 20
        };

        this.miVisor = L.map("mapaid");
        this.miVisor.setView(this.posicionInicial, this.escalaInicial);
        this.mapaBase = L.tileLayer(this.proveedorURL, this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }

    colocarMarcador(posicion) {
        this.marcadores.push(L.marker(posicion)); //agregar elementos al arreglo (push)
        this.marcadores[this.marcadores.length - 1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion) {
        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length - 1].addTo(this.miVisor);
    }

    colocarPoligono(posicion1) {
        this.poligonos.push(L.polygon(posicion1));
        this.poligonos[this.poligonos.length - 1].addTo(this.miVisor);
    }
}

let miMapa = new Mapa();


//marcador de mi casa
miMapa.colocarMarcador([4.698256, -74.125811]);

//barrio florida blanca
miMapa.colocarCirculo([4.696193, -74.112074], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 400
});

//centrocomercial diverplaza
miMapa.colocarPoligono(
    [
        [4.699770, -74.115067],
        [4.701304, -74.113855],
        [4.702095, -74.116741],
        [4.701507, -74.117149]
    ]
);